package com.lambda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LambdaApiSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(LambdaApiSpringApplication.class, args);
	}
}
