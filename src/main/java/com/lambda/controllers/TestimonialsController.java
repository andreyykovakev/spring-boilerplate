package com.lambda.controllers;

import com.lambda.database.models.Testimonial;
import com.lambda.database.repositories.JdbcTestimonialRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author andreikovalev on 5/13/19
 * @project lambda-api-spring
 */

@RestController
@RequestMapping(path = "/testimonials",
        produces = "application/json")
@CrossOrigin(origins = "*")
public class TestimonialsController {

    private JdbcTestimonialRepository testimonialRepository;

    @Autowired
    public TestimonialsController(@Qualifier("mysql-testimonial-repository") JdbcTestimonialRepository testimonialRepository) {
        this.testimonialRepository = testimonialRepository;
    }

    @GetMapping(path = "/")
    public List<Testimonial> getAll() {
        return testimonialRepository.findMany();
    }

    @GetMapping(path = "/{id}")
    public Testimonial getTestimonial(@PathVariable int id) {
        return testimonialRepository.findOne(id);
    }

    @PostMapping(consumes="application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public Testimonial postTestimonial(@RequestBody Testimonial testimonial) {
        return testimonialRepository.save(testimonial);
    }

    @PutMapping(path = "/", consumes="application/json")
    public Testimonial putTestimonial(@RequestBody Testimonial testimonial) {
        return testimonialRepository.update(testimonial.getId(), testimonial);
    }

    @DeleteMapping(path = "/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteTestimonial(@PathVariable int id) {
        testimonialRepository.delete(id);
    }


}
