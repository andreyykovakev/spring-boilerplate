package com.lambda.database.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.springframework.data.rest.core.annotation.RestResource;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * @author andreikovalev on 5/16/19
 * @project lambda-api-spring
 */

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
@Entity
@Table(name = "jobs")
@RestResource(rel="jobs", path="jobs")
public class Job {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "job_id")
    private Integer id;
    private String position;
    private String schedule;
    private String location;
    private String salary;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "job_id")
    private List<JobDescription> description;
    @JsonProperty("date_created")
    @Column(name = "date_created")
    private Date dateCreated;
    @Column(name = "date_updated")
    @JsonProperty("date_updated")
    private Date dateUpdated;
}
