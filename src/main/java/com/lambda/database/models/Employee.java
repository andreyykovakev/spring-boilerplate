package com.lambda.database.models;

import lombok.*;
import org.springframework.data.rest.core.annotation.RestResource;

import javax.persistence.*;


/**
 * @author andreikovalev on 4/2/19
 * @project lambda-direct-admin-api
 */

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
@Entity
@Table(name = "employees")
@RestResource(rel="employees", path="employees")
public class Employee {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;
    private String position;
    private String name;
    private String imageUrl;
}
