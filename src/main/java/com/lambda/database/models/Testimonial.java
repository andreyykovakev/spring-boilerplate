package com.lambda.database.models;

import java.util.Objects;

public class Testimonial {
    private int id;
    private String name;
    private String position;
    private String testimonial;
    private String path;

    public Testimonial(int id,
                       String name,
                       String position,
                       String testimonial,
                       String path) {
        this.id = id;
        this.name = name;
        this.position = position;
        this.testimonial = testimonial;
        this.path = path;
    }

    public Testimonial(String name,
                       String position,
                       String testimonial,
                       String path) {
        this.name = name;
        this.position = position;
        this.testimonial = testimonial;
        this.path = path;
    }

    public Testimonial() {
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPosition() {
        return position;
    }

    public String getTestimonial() {
        return testimonial;
    }

    public String getPath() {
        return path;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Testimonial that = (Testimonial) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(position, that.position) &&
                Objects.equals(testimonial, that.testimonial) &&
                Objects.equals(path, that.path);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, position, testimonial, path);
    }

    @Override
    public String toString() {
        return "Testimonial{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", position='" + position + '\'' +
                ", testimonial='" + testimonial + '\'' +
                ", path='" + path + '\'' +
                '}';
    }
}
