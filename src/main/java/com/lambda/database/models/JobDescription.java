package com.lambda.database.models;

import lombok.*;

import javax.persistence.*;

/**
 * @author andreikovalev on 5/16/19
 * @project lambda-api-spring
 */

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
@Entity(name = "Description")
@Table(name = "job_description")
public class JobDescription {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "job_description_id")
    private Integer id;
    private String description;

}
