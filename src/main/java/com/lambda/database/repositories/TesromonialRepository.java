package com.lambda.database.repositories;

import com.lambda.database.models.Testimonial;

/**
 * @author andreikovalev on 5/8/19
 * @project lambda-api-spring
 */
public interface TesromonialRepository {

    Testimonial save(Testimonial testimonial);
}
