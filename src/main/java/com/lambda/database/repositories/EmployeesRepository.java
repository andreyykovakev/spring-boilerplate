package com.lambda.database.repositories;

import com.lambda.database.models.Employee;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author andreikovalev on 5/14/19
 * @project lambda-api-spring
 */

@Repository
public interface EmployeesRepository extends CrudRepository<Employee, Integer> {
}
