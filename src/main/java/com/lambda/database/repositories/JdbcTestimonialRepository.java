package com.lambda.database.repositories;

import com.lambda.database.models.Testimonial;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author andreikovalev on 5/8/19
 * @project lambda-api-spring
 */

@Repository
public class JdbcTestimonialRepository implements TesromonialRepository {


    private JdbcTemplate jdbc;
    //private SimpleJdbcInsert jdbcInsert;

    @Autowired
    public JdbcTestimonialRepository(@Qualifier("mysqlJdbcTemplate") JdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }

    @Override
    public Testimonial save(Testimonial testimonial) {
        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbc.update(connection -> getPreparedStatementFor(testimonial, connection), keyHolder);

        return findOne(extractGeneratedId(keyHolder));
    }

    private PreparedStatement getPreparedStatementFor(Testimonial testimonial, Connection connection) throws SQLException {
        PreparedStatement ps = connection.prepareStatement("insert into testimonials (name, position, testimonial, path) values (?, ?, ?, ?)",
                new String[]{"name", "position", "testimonial", "path"});

        ps.setString(1, testimonial.getName());
        ps.setString(2, testimonial.getPosition());
        ps.setString(3, testimonial.getTestimonial());
        ps.setString(4, testimonial.getPath());
        return ps;
    }

    private int extractGeneratedId(KeyHolder keyHolder) {
        Number key = keyHolder.getKey();

        if (key == null) {
            throw new RuntimeException("No returned id from database");
        }

        return key.intValue();
    }

    public Testimonial findOne(int id) {
        return jdbc.queryForObject(
                "select * from testimonials where id=?", this::mapRow, id);
    }

    public List<Testimonial> findMany() {
        List<Map<String, Object>> rows = jdbc.queryForList("select * from testimonials");
        return mapRows(rows);

    }

    private List<Testimonial> mapRows(List<Map<String, Object>> rows) {
        List<Testimonial> out = new ArrayList<>();

        for (Map<String, Object> row : rows) {
            int id = Integer.parseInt(row.get("id").toString());
            String name = (String) row.get("name");
            String position = (String) row.get("position");
            String testimonialStr = (String) row.get("testimonial");
            String path = (String) row.get("path");

            out.add(new Testimonial(id, name, position, testimonialStr, path));
        }

        return out;
    }

    private Testimonial mapRow(ResultSet rs, int rowNum) throws SQLException {
        int id = rs.getInt("id");
        String name = rs.getString("name");
        String position = rs.getString("position");
        String testimonialStr = rs.getString("testimonial");
        String path = rs.getString("path");

        return new Testimonial(id, name, position, testimonialStr, path);
    }

    public Testimonial update(int id, Testimonial testimonial) {
        jdbc.update(
                "update testimonials set name = ?, position = ?, testimonial = ?, path = ? where id = ?",
                testimonial.getName(),
                testimonial.getPosition(),
                testimonial.getTestimonial(),
                testimonial.getPath(), id);

        return findOne(id);
    }

    public void delete(int id) {
        jdbc.update("DELETE FROM testimonials WHERE id = ?", id);
    }

    public void deleteMany() {
        jdbc.update("DELETE FROM testimonials");
    }
}
