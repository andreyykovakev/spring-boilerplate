package com.lambda.database.repositories;

import com.lambda.database.models.Job;
import org.springframework.data.repository.CrudRepository;

/**
 * @author andreikovalev on 5/16/19
 * @project lambda-api-spring
 */
public interface JobsRepository extends CrudRepository<Job, Integer> {
}
