CREATE DATABASE IF NOT EXISTS lambda_api;

use lambda_api;

create table if not exists testimonials (
id varchar(255) not null,
name varchar(255) not null,
position varchar(255) not null,
testimonial varchar(255) not null,
path varchar(255) not null

);
