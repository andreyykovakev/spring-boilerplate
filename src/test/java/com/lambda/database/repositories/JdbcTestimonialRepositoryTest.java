package com.lambda.database.repositories;

import com.lambda.LambdaApiSpringApplication;
import com.lambda.database.models.Testimonial;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @author andreikovalev on 5/8/19
 * @project lambda-api-spring
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = LambdaApiSpringApplication.class)
//@RunWith(SpringRunner.class)
//@ContextConfiguration(classes = AppConfig.class)
public class JdbcTestimonialRepositoryTest {

    private static final Testimonial TEST_TESTIMONIAL = new Testimonial("testName", "testPosition", "testTestim", "testPath");
    private static final Testimonial TEST_UPDATE_TESTIMONIAL = new Testimonial("testUpdated", "testUpdated", "testUpdated", "testUpdated");

    @Qualifier("h2-testimonial-repository")
    @Autowired
    private JdbcTestimonialRepository repository;


    @Before
    public void init() {
        repository.deleteMany();
    }


    @Test
    public void save() {
        repository.save(TEST_TESTIMONIAL);

        List<Testimonial> testimonials = repository.findMany();
        Testimonial testimonialDb = testimonials.get(0);

        checkInsertedEqualsSelectedFromDb(testimonialDb);
    }

    @Test
    public void findById(){
        repository.save(TEST_TESTIMONIAL);

        List<Testimonial> testimonials = repository.findMany();
        Testimonial testimonialDb = testimonials.get(0);
        Testimonial findOneResult = repository.findOne(testimonialDb.getId());

        checkInsertedEqualsSelectedFromDb(findOneResult);
    }

    @Test
    public void update(){
        repository.save(TEST_TESTIMONIAL);

        Testimonial testimonialDb = repository.findMany().get(0);

        Testimonial testimonialUpdated = repository.update(testimonialDb.getId(), TEST_UPDATE_TESTIMONIAL);

        Assert.assertEquals(TEST_UPDATE_TESTIMONIAL.getName(), testimonialUpdated.getName());
        Assert.assertEquals(TEST_UPDATE_TESTIMONIAL.getPath(), testimonialUpdated.getPath());
        Assert.assertEquals(TEST_UPDATE_TESTIMONIAL.getPosition(), testimonialUpdated.getPosition());
        Assert.assertEquals(TEST_UPDATE_TESTIMONIAL.getTestimonial(), testimonialUpdated.getTestimonial());
    }

    @Test
    public void delete(){
        repository.save(TEST_TESTIMONIAL);
        repository.delete(1);
        List<Testimonial> testimonials = repository.findMany();

        Assert.assertTrue(testimonials.isEmpty());
    }

    @Test
    public void deleteMany(){
        repository.save(TEST_TESTIMONIAL);
        repository.deleteMany();
        List<Testimonial> testimonials = repository.findMany();

        Assert.assertTrue(testimonials.isEmpty());
    }

    @Test
    public void findMany(){
        repository.save(TEST_TESTIMONIAL);
        List<Testimonial> testimonials = repository.findMany();

        Testimonial testimonialDb = testimonials.get(0);

        checkInsertedEqualsSelectedFromDb(testimonialDb);
    }

    private void checkInsertedEqualsSelectedFromDb(Testimonial testimonialDb) {
        Assert.assertEquals(TEST_TESTIMONIAL.getName(), testimonialDb.getName());
        Assert.assertEquals(TEST_TESTIMONIAL.getPath(), testimonialDb.getPath());
        Assert.assertEquals(TEST_TESTIMONIAL.getPosition(), testimonialDb.getPosition());
        Assert.assertEquals(TEST_TESTIMONIAL.getTestimonial(), testimonialDb.getTestimonial());
    }
}