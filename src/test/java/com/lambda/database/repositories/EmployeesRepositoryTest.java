package com.lambda.database.repositories;

import com.lambda.configuration.AppConfig;
import com.lambda.database.models.Employee;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * @author andreikovalev on 5/14/19
 * @project lambda-api-spring
 */

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = AppConfig.class)
@EnableJpaRepositories(basePackages = "com.lambda.database")
public class EmployeesRepositoryTest {

    @Resource
    private EmployeesRepository repository;

    @Before
    public void init() {


    }


    @Test
    public void save() {
        Employee employee = Employee.builder()
                .name("testName")
                .position("position")
                .imageUrl("imgUrl")
                .build();

    }


}